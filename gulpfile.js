'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');

var paths = {
  loadPaths: ['./assets/**/*.sass']
};

gulp.task('css', function () {
  return gulp.src(paths.loadPaths)
    .pipe(sass().on('error', sass.logError))
    .pipe(cssnano())
    .pipe(gulp.dest('public/assets'));
});

gulp.task('watch', function () {
  gulp.watch([paths.loadPaths], ['css']);
});
